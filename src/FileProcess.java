import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

import model.Company;
import models.CompanyDao;
import models.CompanyModel;


public class FileProcess {

	private String pathFiles;
	private List<String> listFiles;  
	
	public FileProcess(String pathsFiles){
		this.pathFiles = pathsFiles;
		listFiles = new ArrayList<String>();
	}
	
	public void listFilesForFolder() {
	    File[] files = new File(pathFiles).listFiles();
	    //If this pathname does not denote a directory, then listFiles() returns null. 

	    for(File file : files){
	        if(file.isFile()){
	        	if(file.getName().endsWith(".xlsx") || file.getName().endsWith(".xls")){
	        		listFiles.add(file.getName());
	        	}
	        }
	    }
	}
	
	public void printFiles(){
		for(String file : listFiles){
			System.out.println(file);
		}
	}
	
	public void proveAlgorithm(){
		listFilesForFolder();
		//printFiles();
		int fileFail = 0;
		int fileSucceed = 0;
		int totalFiles = 0;
		int realSucceed = 0;
		for(String file : listFiles){
			totalFiles++;
			/*
			Parse reader = new Parse();
			String datas = reader.getContentExcel(file);
			reader.printObjectModel();
			reader.mutchAreIquals();
			*/
			try{
				System.out.println("#######################VERSION TO File "+ file +"#######################################");
				Parse reader = new Parse();
				String datas = reader.getContentExcel(file);
				reader.printObjectModel();
				reader.mutchAreIquals();
				if(reader.fileSucceed()){
					realSucceed++;
				}
				fileSucceed++;
			}catch(Exception e){
				fileFail++;
				System.err.println("Has error in file "+file);
			}
		}
		System.out.println("Total files: "+totalFiles);
		System.out.println("Files succeed: "+fileSucceed);
		System.out.println("Files succeed: "+realSucceed);
		System.out.println("Files failed: "+fileFail);
		
	}
	
	public void filesProcess(){
		Parse reader = new Parse();
		String datas = reader.getContentExcel("C_120_D114_0.xlsx");
		
		Company company = reader.getCompany();
		CompanyDao companyDao = new CompanyDao();
		CompanyModel companyModel = new CompanyModel("No Named Yet");
		
		companyModel.setNameFile("C_120_D114_0.xlsx");
		companyModel.setFirstManagementYear(company.getFisrtYear());
		companyModel.setSecondManagementYear(company.getSecondYear());
		
		companyModel.addTotalAssetsName(company.getTotalAssetsName());
		companyModel.addTotalAssetsFirstManagement(company.getTotalAssestsFirstManagement());
		companyModel.addTotalAssetsSecondManagement(company.getTotalAssetsSecondManagement());
		
		companyModel.addTotalLiabilitiesName(company.getTotalLiabilitiesName());
		companyModel.addTotalLiabilitiesFirstManagement(company.getTotalLiabilitiesFirstManagement());
		companyModel.addTotalLiabilitiesSecondManagement(company.getTotalLiabilitiesSecondManagement());
		
		companyModel.addNetAssetName(company.getNetAssetsName());
		companyModel.addNetAssetsFirstManagement(company.getNetAssetsFirstManagement());
		companyModel.addNetAssetsSecondManagement(company.getNetAssetsSecondManagement());
		
		companyModel.addTotalEquityName(company.getTotalEquityName());
		companyModel.addTotalEquityFirstManagement(company.getTotalEquityFirstYear());
		companyModel.addTotalAssetsSecondManagement(company.getTotalLiabilitiesSecondManagement());
		
		
	}
	private void fillCurrentAsset(Company company, CompanyModel companyModel){
		LinkedHashMap<String,String> firstYearCurrentAsset = company.getFirstYearCurrentAsset();
		 if(firstYearCurrentAsset != null ){
				if(!firstYearCurrentAsset.isEmpty()){
					Set keys = firstYearCurrentAsset.keySet();
					Iterator ite = keys.iterator(); 
					while(ite.hasNext()){
						String name = ite.next().toString();
				        System.out.print(name);
				        System.out.println(" " + firstYearCurrentAsset.get(name));
					}
				}else{
					System.out.println("Your Current Assets is empty!!");
				}
			}
	}
}
