import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Company;
import model.Management;
import model.NetAssets;
import model.TotalAssets;
import model.TotalLiabilities;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Parse {

	private String directory;
	private Company company;
	private boolean keepGoing;
	
	private String [][] financialPosition;
	
	private String [] nonCurrentAssetsWords;
	private String [] currentAssetsWords;
	
	private String [] totalAssetsWords;
	
	private String [] currentLiabilitiesWords;
	
	private String [] totalCurrentLiabilitiesWords;
	
	private String [] nonCurrentLiabilitiesWords;
	
	private String [] equityWords;
	
	private String [] finalEquityLiability;
	
	private String [] equityTittles;
	
	Financial whichProcess = Financial.NON_MATCH;
	
	private ArrayList<String> mustMutch = new ArrayList<String>();
	private ArrayList<String> justMutch = new ArrayList<String>();
	
	private String[][] grammarReference;
	
	
	public Parse(){
		keepGoing = true;
		financialPosition = new String [100][7];
		fillNonCurrentAssetsWords();
		fillFinalIquityLiability();
		fillCurrentAssets();
		fillCurrentLiabilitiesWords();
		fillTotalAssets();
		fillEquityWords();
		fillNonCurrentLiabilitiesWords();
		fillTotalCurrentLiabilitiesWords();
		fillEquityTittles();
	}
	
	public void fillEquityWords(){
		equityWords = new String[]
						{"Share Capital"
	              		,"Revaluation reserves"
	              		,"Revaluation reserve"
	              		,"Retained earnings"
	              		,"Capital reserve"
	              		,"Accumulated losses"
	              		,"Other reserves"
						};
	}
	
	public void fillNonCurrentAssetsWords(){
		nonCurrentAssetsWords = new String []
								{"Investment propertiy"
								,"Property, plant and equipment"
								,"Investments carried at cost"
								,"Investments in subsidiaries"
								,"Investments - available-for-sale"
								,"Total non-current assets"
								,"Investment in subsidiaries, associated and related companies"
								,"Deferred tax asset"
								,"Deferred taxation"
								};
	}
	
	public void fillCurrentAssets(){
		currentAssetsWords = new String []
				{ "Inventories"
                , "Trade and other receivables"
                , "Cash and cash equivalents"
                , "Cash and bank balances"
                , "Current tax assets"
                , "Current tax"
                , "Investments - loans and receivables"
                , "Total current assets"
                //, "Total assets"
                , "Amount due from related companies"
                , "Taxation refundable"
                , "Cash at bank"
                , "Amounts owed by group undertakings"
				};
	}
	
	public void fillTotalAssets(){
		totalAssetsWords = new String[]
				{ "Total Assets"
				};
	}
	
	
	public void fillCurrentLiabilitiesWords(){
		currentLiabilitiesWords = new String[]
				{ "Trade and other payables"
                , "Trade & Other payables"
                , "Other financial liabilities"
                , "Current tax liabilities"
                , "Borrowings"
				};
	}
	
	public void fillTotalCurrentLiabilitiesWords(){
		totalCurrentLiabilitiesWords = new String[]
				{ "Total current liabilities"
                , "Total liabilities"
                , "Total equity and liabilities"
				};
	}
	
	public void fillNonCurrentLiabilitiesWords(){
		nonCurrentLiabilitiesWords = new String[]
				{ "Investment propertiy"
                , "Property, plant and equipment"
                , "Investments carried at cost"
                , "Investments in subsidiaries"
                , "Investments - available-for-sale"
                , "Total non-current assets"
                , "Investment in subsidiaries, associated and related companies"
                , "Deferred tax asset"
                , "Deferred tax liabilities"
                , "Long-term borrowgins"
                , "Other provisions"
                , "Total non-current liabilities"
				};
	}
	
	public void fillFinalIquityLiability(){
		finalEquityLiability = new String []
				{"Total Shareholder Equity"
				,"Total equity"
				};
	}
	
	public void fillEquityTittles(){
		equityTittles = new String []
				{ "EQUITY"
				, "Capital and reserves"
				};
	}
	
	public void fillGrammarReference(){
		grammarReference = new String[][]{
				{"ASSETS AND LIABILITES",null,null},
				
				{"noCurrentAsset","firstYear","secondYear"},
				{"totalNoCurrent","firstYear","secondYear"},
				
				{"currentAsset","firstYear","secondYear"},
				{"totalCurrent","firstYear","secondYear"},
				
				{"totalAssets","firstYear","secondYear"},
				
				{"currentLiabilities","firstYear","secondYear"},
				{"totalCurrentLiabilities","firstYear","secondYear"},
				
				{"nonCurrentLiabilities","firstYear","secondYear"},
				{"totalNonCurrentLiabilities","firstYear","secondYear"},
				
				{"totalLiabilites","firstYear","secondYear"},
				
				{"netAssets","firstYear","secondYear"},
				
				{"EQUITY",null,null},
				{"Equity","firstYear","secondYear"},
				{"totalEquity","firstYear","secondYear"},
				
		};
	}
	
	/*
	public Parse(String directory){
		this.directory = directory;
	}
	*/
	
	public Company getCompany(){
		return company;
	}
	
	public void setDirectory(String directory){
		this.directory = directory;
	}
	/**
	 * 
	 * @return a directory path in which is working this parse  
	 */
	public String getDirectory(){
		return directory;
	}
	
	/**
	 * 
	 * @param fileName  any name file to verify if there is in the temp directory
	 * @return          if exist the file in the path /temp
	 */
	public boolean thereIsFileExcel(String fileName){
		
		File dir = new File("");
	    String so = System.getProperty("os.name");
	    String tem = "";
	    
	    if(so.contains("Linux")){
	       	tem = "/temp/";
	    }
	    if(so.contains("Windows")){
	      	tem = "\\temp\\";
	    }
	    
	    setDirectory(dir.getAbsolutePath()+tem);
	    File documento = new File(getDirectory()+fileName);
	    
	    return documento.exists();
	}
	
	/**
	 * @param nameFile just a name of excel file 
	 * @return if the file is a document excel or not 
	 */
	public boolean isExcelFile(String fileName){		
		return fileName.endsWith(".xlsx") || fileName.endsWith(".xls");
	}
	
	/**
	 * 
	 * @param nameFile
	 * @return a content with all data from excel file
	 */
	public String getContentExcel(String fileName){
		String content = "";
		if(isExcelFile(fileName) && thereIsFileExcel(fileName)){
			try{
				File document = new File(directory+fileName);
				FileInputStream file = new FileInputStream(document);
				
				//Get the workbook instance for XLS file 
				XSSFWorkbook workbook = new XSSFWorkbook (file);
				
				//Get first sheet from the workbook
				XSSFSheet sheet = workbook.getSheetAt(0);
				
				//Get iterator to all the rows in current sheet
				Iterator<Row> rowIterator = sheet.iterator();
				
				boolean foundStart = false;
				
				while(rowIterator.hasNext() && keepGoing){
					Row row = rowIterator.next();
					row.getLastCellNum();
					row.getFirstCellNum();
					//For each row, iterate through each columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
					while(cellIterator.hasNext()){
						Cell cell = cellIterator.next();
						switch(cell.getCellType()){
							case Cell.CELL_TYPE_BLANK:
								if(foundStart){
									content += "NADA\t\t";
								}
								break;
							case Cell.CELL_TYPE_BOOLEAN:
								if(foundStart){
									content += cell.getBooleanCellValue() + "\t\t";
								}
								break;
							case Cell.CELL_TYPE_NUMERIC:
								if(foundStart){
									content += cell.getNumericCellValue() + "\t\t";
								}
								break;
							case Cell.CELL_TYPE_STRING:
								if(isStatementFinancial(cell.getStringCellValue()) && !cellIterator.hasNext()){
									foundStart = true;
									content += cell.getStringCellValue() + "\t\t";
									matchGrammar(rowIterator);
								}else{
									if(foundStart){
										content += cell.getStringCellValue() + "\t\t";
									}
								}
								break;
							default:
								content += "[NO TYPE SUPPORTED]";
						}
					}
					if(foundStart){
						content += "\n";
					}
				}
				file.close();
				
			}catch(FileNotFoundException e){
				e.printStackTrace();
			}catch (IOException e) {
			    e.printStackTrace();
			    System.out.println("Ups! we have a problem with the directory "+getDirectory());
			}
		}
		return content;
	}
	
	public boolean isStatementFinancial(String words){
		return DistanceCalculator.getSimilarity("Statement of Financial Position",words) >= 0.8;
	}
	
	public void matchGrammar(Iterator<Row> rowIterator){
		String content = "@@@@@@ START MATCH GRAMMAR @@@@@";
		boolean foundStart = true;
		//boolean savePrevious = true;
		while(rowIterator.hasNext() && keepGoing){
			Row row = rowIterator.next();
			/*
			if(savePrevious){
				Row rowPrevious;
			}
			*/
			//For each row, iterate through each columns
			/*
			Iterator<Cell> cellIterator = row.cellIterator();
			while(cellIterator.hasNext()){
				Cell cell = cellIterator.next();
			*/
			for (int c = 0; c < row.getLastCellNum(); c++) {
		        Cell cell = row.getCell(c,Row.CREATE_NULL_AS_BLANK);
				if(cell.getCellType() == Cell.CELL_TYPE_STRING){
					if(DistanceCalculator.getSimilarity(cell.getStringCellValue(), "Notes") >= 0.8){
						Cell firstYear = c+1 < row.getLastCellNum() ? row.getCell(c+1) : null;
						Cell secondYear = c+2 < row.getLastCellNum() ? row.getCell(c+2) : null;
						/*
						if(firstYear == null && rowPrevious != null){
							System.out.println("Son nulos");
							
						}
						*/
						
						if((firstYear.getCellType() == Cell.CELL_TYPE_NUMERIC || 
							firstYear.getCellType() == Cell.CELL_TYPE_STRING )
							&&  
						    secondYear.getCellType() == Cell.CELL_TYPE_NUMERIC)
						{	
							foundStart = false;
							
							int secYear = (int)secondYear.getNumericCellValue();
							int firYear = firstYear.getCellType() == Cell.CELL_TYPE_STRING ? secYear+1 : (int)secondYear.getNumericCellValue(); 
							
						    
						    company = new Company("No Name",2);
						    
						    company.setFirstYear(firYear);
						    company.setSecondYear(secYear);
						    
						    company.setTwogestionsPostions(c+1, c+2);
						    //keepGoing = false;
						    if(firstYear.getCellType() == Cell.CELL_TYPE_STRING){
						    	content += firstYear.getStringCellValue() + "\t\t";
							    content += secondYear.getNumericCellValue() + "\t\t";
							}else{
								content += firstYear.getNumericCellValue() + "\t\t";
							    content += secondYear.getNumericCellValue() + "\t\t";
							}
						    matchAssetLiabilities(rowIterator);
						    //fillTable(rowIterator);
						}else{
							if(firstYear.getCellType() == Cell.CELL_TYPE_NUMERIC){
								/**
								 * @TODO think about how to handle when the finance is for a year 
								 * 
								 */
							}else{
								/**
								 * @TODO looking for a message to exit 
								 * 
								 */
								keepGoing = false;
							}
						}
					}
				}
			}
//			rowPrevious = row;
			if(foundStart){
				content += "\n";
			}else{
				break;
			}
		}
		//System.out.println(content + "@@@@@@ END GRAMMAR @@@@@\n");
	}


	public void matchAssetLiabilities(Iterator<Row> rowIterator){
		String content = "@@@@@@ START MATCH ASSET LEABILITIES@@@@@\n";
		while(rowIterator.hasNext() && keepGoing){
			Row row = rowIterator.next();
			//For each row, iterate through each columns
			for (int c = 0; c < row.getLastCellNum(); c++) {
		        Cell cell = row.getCell(c,Row.CREATE_NULL_AS_BLANK);
				if(cell.getCellType() == Cell.CELL_TYPE_STRING){
					if(areEqualsWords(cell.getStringCellValue(),"ASSETS AND LIABILITIES")){
						content += cell.getStringCellValue();
						whichProcess = Financial.ASSETS_AND_LIABILITIES;
						fillTable(rowIterator);
						break;
						//keepGoing = false;
					}
					if(areEqualsWords(cell.getStringCellValue(),"ASSETS")){
							content += cell.getStringCellValue();
							whichProcess = Financial.ASSETS;
							fillTable(rowIterator);
							break;
							//keepGoing = false;
					}
					if(areEqualsWords(cell.getStringCellValue(),"Non-current assets")){
						content += cell.getStringCellValue();
						whichProcess = Financial.NON_CURRENT_ASSETS;
						fillTable(rowIterator);
						break;
						//keepGoing = false;
					} 
				}
			}
			content += "\n";
		}
		//System.out.println(content + "@@@@@@ END ASSET LEABILTY@@@@@");
	}

	
	public void fillTable(Iterator<Row> rowIterator){
		int posX = 0;
		int posY = 0; 
		
		DataFormatter fmt = new DataFormatter();
		
		while(rowIterator.hasNext() && keepGoing){
			Row row = rowIterator.next();
			
			//For each row, iterate through each columns
			/*
			Iterator<Cell> cellIterator = row.cellIterator();
			while(cellIterator.hasNext()){
			*/
			for (int c = 0; c < row.getLastCellNum(); c++) {
		        Cell cell = row.getCell(c,Row.CREATE_NULL_AS_BLANK);
				//Cell cell = cellIterator.next();
				switch(cell.getCellType()){
					case Cell.CELL_TYPE_ERROR:
						financialPosition[posX][posY] = "blank";
						break;
					case Cell.CELL_TYPE_BLANK:
						financialPosition[posX][posY] = "blank";
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						financialPosition[posX][posY] = "blank";
						break;
					case Cell.CELL_TYPE_FORMULA:
						financialPosition[posX][posY] = "blank";
						break;
					case Cell.CELL_TYPE_NUMERIC:
						financialPosition[posX][posY] = fmt.formatCellValue(cell);
						//financialPosition[posX][posY] = cell.getStringCellValue();
						break;
					case Cell.CELL_TYPE_STRING:
						switch(whichProcess){
							case ASSETS_AND_LIABILITIES :
								if(hasFinalIquityLiability(cell.getStringCellValue())){
									
									financialPosition[posX][posY] = cell.getStringCellValue();
									
									Cell firstValue = c+1 < row.getLastCellNum() ? row.getCell(c+1) : null; 
									Cell secondValue = c+2 < row.getLastCellNum() ? row.getCell(c+2) : null;
									if(firstValue != null && secondValue != null
										&& firstValue.getCellType() == Cell.CELL_TYPE_NUMERIC
										&& secondValue.getCellType() == Cell.CELL_TYPE_NUMERIC)
									{
										financialPosition[posX][posY+1] = firstValue.getNumericCellValue() + "";
										financialPosition[posX][posY+2] = secondValue.getNumericCellValue() + "";
										posY+=2;
									}else{
										/**
										 * @TODO Think in other cases
										 */
										if(firstValue != null && firstValue.getCellType() == Cell.CELL_TYPE_NUMERIC){
											financialPosition[posX][posY+1] = firstValue.getNumericCellValue() + "";
											posY++;
										}
									}
									keepGoing = false;
								}else{
									financialPosition[posX][posY] = cell.getStringCellValue();
								}
								break;
							case ASSETS:
								//System.out.println("Entra desde aquiiiiii ASSETS");
								if(hasWordOnWordsDictionary(cell.getStringCellValue(),totalCurrentLiabilitiesWords)){
									financialPosition[posX][posY] = cell.getStringCellValue();
									
									Cell firstValue = c+1 < row.getLastCellNum() ? row.getCell(c+1) : null; 
									Cell secondValue = c+2 < row.getLastCellNum() ? row.getCell(c+2) : null;
									if(firstValue != null && secondValue != null
										&& firstValue.getCellType() == Cell.CELL_TYPE_NUMERIC
										&& secondValue.getCellType() == Cell.CELL_TYPE_NUMERIC)
									{
										financialPosition[posX][posY+1] = firstValue.getNumericCellValue() + "";
										financialPosition[posX][posY+2] = secondValue.getNumericCellValue() + "";
										posY+=2;
									}else{
										/**
										 * @TODO Think in other cases
										 */
										if(firstValue != null && firstValue.getCellType() == Cell.CELL_TYPE_NUMERIC){
											financialPosition[posX][posY+1] = firstValue.getNumericCellValue() + "";
											posY++;
										}
									}
									keepGoing = false;
								}else{
									financialPosition[posX][posY] = cell.getStringCellValue();
								}
								break;
							case NON_MATCH:
								System.out.println("THERE IS NOT MATCH ANY PATTERN");
								break;
						}
						
				}
				posY++;
			}
			posY=0;
			posX++;
		}
	}
	
	public boolean hasNonCurrentAssetsWord(String word){
		boolean yes=false;
		for(int i=0;i<nonCurrentAssetsWords.length;i++){
			if(DistanceCalculator.getSimilarity(word, nonCurrentAssetsWords[i])>=0.85){
				yes = true;
				break;
			}
		}
		return yes;
	}
	
	public boolean hasCurrentAssetsWord(String word){
		boolean yes=false;
		for(int i=0;i<currentAssetsWords.length;i++){
			if(DistanceCalculator.getSimilarity(word, currentAssetsWords[i])>=0.85){
				yes = true;
				break;
			}
		}
		return yes;
	}
	
	public boolean hasFinalIquityLiability(String word){
		boolean yes = false;
		for(int i=0;i<finalEquityLiability.length;i++){
			if(DistanceCalculator.getSimilarity(word,finalEquityLiability[i]) > 0.85){
				yes = true;
				break;
			}
		}
		return yes;
	}
	
	public boolean hasWordOnWordsDictionary(String word,String [] dictionary){
		boolean yes = false;
		for(int i=0;i<dictionary.length;i++){
			if(DistanceCalculator.getSimilarity(word,dictionary[i]) >= 0.85){
				yes = true;
				break;
			}
		}
		return yes;
	}
	
	public void printFinalIquityLiability(){
		for(int i=0;i<financialPosition.length;i++){
			for(int j=0;j<financialPosition[0].length;j++){
				System.out.print(financialPosition[i][j]+"\t\t");
				/*if(financialPosition[i][j] != null){
					System.out.print(financialPosition[i][j]+"\t\t");
				}else{
					break;
				}
				*/
			}
			if(financialPosition[i][0] != null){
				System.out.println();
			}else{
				break;
			}
		}
	}
	public void matchDataToCompare(){
		for(int i=0;i<financialPosition.length;i++){
			for(int j=0;j<financialPosition[0].length;j++){
				if(financialPosition[i][j] != null){
					if(hasWordOnWordsDictionary(financialPosition[i][j],nonCurrentAssetsWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],currentAssetsWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],totalAssetsWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],currentLiabilitiesWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],totalCurrentLiabilitiesWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],nonCurrentLiabilitiesWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],equityWords)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],finalEquityLiability)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
					if(hasWordOnWordsDictionary(financialPosition[i][j],equityTittles)){
						mustMutch.add(financialPosition[i][j]);
						break;
					}
				}
			}
		}
	}
	public void buildObjectData(){
		matchDataToCompare();
		for(int i=0;i<financialPosition.length;i++){
			for(int j=0;j<financialPosition[0].length;j++){
				String value = financialPosition[i][j] != null ? financialPosition[i][j] : "" ;
				
				/**
				 * This section pick up all non-current assets
				 */
				if(areEqualsWords(value, "Non-current assets")){
					justMutch.add(value);
					i = matchNonCurrentAsset(i,j);
					break;
				}
				/**
				 * This section pick up all current assets
				 */
				if(areEqualsWords(value, "Current assets")){
					justMutch.add(value);
					i = matchCurrentAsset(i,j);
					break;
				}
				if(hasWordOnWordsDictionary(value,totalAssetsWords)){
					justMutch.add(value);
					matchTotalAssets(i,j);
					break;
				}
				if(areEqualsWords(value,"Current liabilities")){
					justMutch.add(value);
					i = matchCurrentLiabilities(i,j);
					break;
				}
				
				if(areEqualsWords(value,"Non-current liabilities")){
					justMutch.add(value);
					i = matchNonCurrentLiabilities(i,j);
					break;
				}
				
				if(hasWordOnWordsDictionary(value,totalCurrentLiabilitiesWords)){
					justMutch.add(value);
					matchTotalLiabilities(i,j);
					break;
				}
				
				if(areEqualsWords(value,"Net assets")){
					justMutch.add(value);
					matchNetAssets(i,j);
					break;
				}
				if(hasWordOnWordsDictionary(value,equityTittles)){
					justMutch.add(value);
					matchEquity(i,j);
					break;
				}
				if(hasWordOnWordsDictionary(value,finalEquityLiability)){
					justMutch.add(value);
					matchTotalEquity(i,j);
					break;
				}
			}
		}
	}
	
	public int matchNonCurrentAsset(int i, int j){
		LinkedHashMap<String,String> firstYear = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> secondYear = new LinkedHashMap<String,String>();
		i++;
		for(;i<financialPosition.length;i++){
			if(hasNonCurrentAssetsWord(financialPosition[i][j])){
				justMutch.add(financialPosition[i][j]);
				if(company.numGestions()==2){
					firstYear.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
					secondYear.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
				}else{
					firstYear.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
				}
			}else{
				/**
				 * @TODO Refacting to support a year management in this moment
				 * only support for two management
				 */
				if(DistanceCalculator.getSimilarity(financialPosition[i][j],"blank") >= 0.8){
					if(i+1<financialPosition.length){
						if(DistanceCalculator.getSimilarity(financialPosition[i+1][j],"Current assets") >= 0.8 
						   || hasCurrentAssetsWord(financialPosition[i+1][j]))
						{
							firstYear.put("Total non-current assets",financialPosition[i][company.getFirstManagementPosition()]);
							secondYear.put("Total non-current assets", financialPosition[i][company.getSecondManagementPosition()]);
							break;
						}else{
							/**
							 * @TODO prove if it is working for other cases
							 */
							firstYear.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
							secondYear.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
						}
					}
				}else{
					i--;
					break;
				}
			}
		}
		company.addFirstYearNonCurrentAsset(firstYear);
		company.addSecondYearNonCurrentAsset(secondYear);
		return i;
	}
	
	public int matchCurrentAsset(int i, int j){
		LinkedHashMap<String,String> firstYearCurrentAssets = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> secondYearCurrentAssets = new LinkedHashMap<String,String>();
		i++;
		for(;i<financialPosition.length;i++){
			if(hasCurrentAssetsWord(financialPosition[i][j])){
				justMutch.add(financialPosition[i][j]);
				if(company.numGestions()==2){
					firstYearCurrentAssets.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
					secondYearCurrentAssets.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
				}else{
					firstYearCurrentAssets.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
				}
			}else{
				/**
				 * @TODO Refacting to support a year management in this moment
				 * only support for two management
				 */
				if(DistanceCalculator.getSimilarity(financialPosition[i][j],"blank") >= 0.8){
					if(i+1<financialPosition.length){
						if(DistanceCalculator.getSimilarity(financialPosition[i+1][j],"Total assets") >= 0.8 
						   || hasWordOnWordsDictionary(financialPosition[i+1][j],currentLiabilitiesWords))
						{
							firstYearCurrentAssets.put("Total current assets",financialPosition[i][company.getFirstManagementPosition()]);
							secondYearCurrentAssets.put("Total current assets", financialPosition[i][company.getSecondManagementPosition()]);
							break;
						}else{
							/**
							 * @TODO prove if it is working for other cases
							 */
							if(j+1<financialPosition[0].length){
								if(hasCurrentAssetsWord(financialPosition[i][j+1])){
									justMutch.add(financialPosition[i][j+1]);
									firstYearCurrentAssets.put(financialPosition[i][j+1],financialPosition[i][company.getFirstManagementPosition()]);
									secondYearCurrentAssets.put(financialPosition[i][j+1], financialPosition[i][company.getSecondManagementPosition()]);
								}else{
									break;
								}
							}else{
								firstYearCurrentAssets.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
								secondYearCurrentAssets.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
							}
						}
					}
				}else{
					i--;
					break;
				}
			}
		}
		company.addFirstYearCurrentAsset(firstYearCurrentAssets);
		company.addSecondYearCurrentAsset(secondYearCurrentAssets);
		return i;
	}
	
	public int matchEquity(int i, int j){
		LinkedHashMap<String,String> firstYearEquity = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> secondYearEquity = new LinkedHashMap<String,String>();
		i++;
		for(;i<financialPosition.length;i++){
			if(hasWordOnWordsDictionary(financialPosition[i][j],equityWords)){
				if(company.numGestions()==2){
					firstYearEquity.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
					secondYearEquity.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
				}else{
					firstYearEquity.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
				}
			}else{
				/**
				 * @TODO Refacting to support a year management in this moment
				 * only support for two management
				 */
				if(areEqualsWords(financialPosition[i][j],"blank")){
					if(i+1<financialPosition.length){
						if(hasWordOnWordsDictionary(financialPosition[i+1][j],finalEquityLiability))
						{
							firstYearEquity.put("Total equity",financialPosition[i][company.getFirstManagementPosition()]);
							secondYearEquity.put("Total equity", financialPosition[i][company.getSecondManagementPosition()]);
							break;
						}else{
							/**
							 * @TODO prove if it is working for other cases
							 */
							if(j+1<financialPosition[0].length){
								if(hasWordOnWordsDictionary(financialPosition[i][j+1],equityWords)){
									firstYearEquity.put(financialPosition[i][j+1],financialPosition[i][company.getFirstManagementPosition()]);
									secondYearEquity.put(financialPosition[i][j+1], financialPosition[i][company.getSecondManagementPosition()]);
								}else{
									break;
								}
							}else{
								firstYearEquity.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
								secondYearEquity.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
							}
						}
					}
				}else{
					i--;
					break;
				}
			}
		}
		company.addFirstYearEquity(firstYearEquity);
		company.addSecondYearEquity(secondYearEquity);
		return i;
	}
	
	public int matchCurrentLiabilities(int i, int j){
		LinkedHashMap<String,String> firstYearCurrentLiabilities = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> secondYearCurrentLiabilities = new LinkedHashMap<String,String>();
		i++;
		for(;i<financialPosition.length;i++){
			if(hasWordOnWordsDictionary(financialPosition[i][j],currentLiabilitiesWords)){
				justMutch.add(financialPosition[i][j]);
				if(company.numGestions()==2){
					firstYearCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
					secondYearCurrentLiabilities.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
				}else{
					firstYearCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
				}
			}else{
				/**
				 * @TODO Refacting to support a year management in this moment
				 * only support for two management
				 */
				if(areEqualsWords(financialPosition[i][j],"blank")){
					if(i+1<financialPosition.length){
						if( hasWordOnWordsDictionary(financialPosition[i+1][j],totalCurrentLiabilitiesWords) 
						   || areEqualsWords(financialPosition[i+1][j],"Net assets")
						   || areEqualsWords(financialPosition[i+1][j],"Non-current liabilities"))
						{
							firstYearCurrentLiabilities.put("Total current assets",financialPosition[i][company.getFirstManagementPosition()]);
							secondYearCurrentLiabilities.put("Total current assets", financialPosition[i][company.getSecondManagementPosition()]);
							break;
						}else{
							/**
							 * @TODO prove if it is working for other cases
							 */
							if(j+1<financialPosition[0].length){
								if(hasCurrentAssetsWord(financialPosition[i][j+1])){
									justMutch.add(financialPosition[i][j]);
									firstYearCurrentLiabilities.put(financialPosition[i][j+1],financialPosition[i][company.getFirstManagementPosition()]);
									secondYearCurrentLiabilities.put(financialPosition[i][j+1], financialPosition[i][company.getSecondManagementPosition()]);
								}else{
									break;
								}
							}else{
								firstYearCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
								secondYearCurrentLiabilities.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
							}
						}
					}
				}else{
					i--;
					break;
				}
			}
		}
		company.addFirstYearCurrentLiabilities(firstYearCurrentLiabilities);
		company.addSecondYearCurrentLiabilities(secondYearCurrentLiabilities);
		return i;
	}

	public int matchNonCurrentLiabilities(int i,int j){
		LinkedHashMap<String,String> firstYearNonCurrentLiabilities = new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> secondYearNonCurrentLiabilities = new LinkedHashMap<String,String>();
		i++;
		for(;i<financialPosition.length;i++){
			//System.out.println("ESTOO LLEGA -->>>>> "+ financialPosition[i][j]);
			if(financialPosition[i][j] != null){
				if(hasWordOnWordsDictionary(financialPosition[i][j],nonCurrentLiabilitiesWords)){
					justMutch.add(financialPosition[i][j]);
					if(company.numGestions()==2){
						firstYearNonCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
						secondYearNonCurrentLiabilities.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
					}else{
						firstYearNonCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
					}
				}else{
					/**
					 * @TODO Refacting to support a year management in this moment
					 * only support for two management
					 */
					if(areEqualsWords(financialPosition[i][j],"blank")){
						if(i+1<financialPosition.length){
							if( hasWordOnWordsDictionary(financialPosition[i+1][j],totalCurrentLiabilitiesWords) 
							   || areEqualsWords(financialPosition[i+1][j],"Total Liabilites"))
							{
								firstYearNonCurrentLiabilities.put("Total current assets",financialPosition[i][company.getFirstManagementPosition()]);
								secondYearNonCurrentLiabilities.put("Total current assets", financialPosition[i][company.getSecondManagementPosition()]);
								break;
							}else{
								/**
								 * @TODO prove if it is working for other cases
								 */
								if(j+1<financialPosition[0].length){
									if(hasCurrentAssetsWord(financialPosition[i][j+1])){
										justMutch.add(financialPosition[i][j]);
										firstYearNonCurrentLiabilities.put(financialPosition[i][j+1],financialPosition[i][company.getFirstManagementPosition()]);
										secondYearNonCurrentLiabilities.put(financialPosition[i][j+1], financialPosition[i][company.getSecondManagementPosition()]);
									}else{
										break;
									}
								}else{
									firstYearNonCurrentLiabilities.put(financialPosition[i][j],financialPosition[i][company.getFirstManagementPosition()]);
									secondYearNonCurrentLiabilities.put(financialPosition[i][j], financialPosition[i][company.getSecondManagementPosition()]);
								}
							}
						}
					}else{
						i--;
						break;
					}
				}
			}else{
				break;
			}
			
		}
		company.addFirstYearNonCurrentLiabilities(firstYearNonCurrentLiabilities);
		company.addSecondYearNonCurrentLiabilities(secondYearNonCurrentLiabilities);
		return i;
	}
	
	public void matchTotalEquity(int i, int j){
		company.setNameTotalEquity(financialPosition[i][j]);
		if(company.numGestions()==2){
			company.setTotalEquityFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
			company.setTotalEquitySecondYear(financialPosition[i][company.getSecondManagementPosition()]);
		}else{
			company.setTotalEquityFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
		}
	}
	
	public void matchTotalAssets(int i, int j){
		TotalAssets totalAsset = new TotalAssets();
		totalAsset.setName(financialPosition[i][j]);
		if(company.numGestions()==2){
			totalAsset.setTotalAssetsFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
			totalAsset.setTotalAssetsSecondYear(financialPosition[i][company.getSecondManagementPosition()]);
		}else{
			totalAsset.setTotalAssetsFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
		}
		company.addTotalAssets(totalAsset);
	}
	
	public void matchNetAssets(int i, int j){
		NetAssets netAsset = new NetAssets();
		netAsset.setName(financialPosition[i][j]);
		if(company.numGestions()==2){
			netAsset.setNetAssetsFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
			netAsset.setNetAssetsSecondYear(financialPosition[i][company.getSecondManagementPosition()]);
		}else{
			netAsset.setNetAssetsFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
		}
		company.addNetAssets(netAsset);
	}
	
	public void matchTotalLiabilities(int i, int j){
		TotalLiabilities totalLiabilities = new TotalLiabilities();
		totalLiabilities.setName(financialPosition[i][j]);
		if(company.numGestions()==2){
			totalLiabilities.setTotalLiabilitiesFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
			totalLiabilities.setTotalLiabilitiesSecondYear(financialPosition[i][company.getSecondManagementPosition()]);
		}else{
			totalLiabilities.setTotalLiabilitiesFirstYear(financialPosition[i][company.getFirstManagementPosition()]);
		}
		company.addTotalLiabilities(totalLiabilities);
	}
	
	public void printObjectModel(){
		buildObjectData();
		
		company.printFirstYearNonCurrentAsset();
		System.out.println();
		company.printSecondYearNonCurrentAsset();
		System.out.println();
		
		company.printLinkedHashMap(company.getFirstYearCurrentAsset(), " First Year Current Assets ");
		System.out.println();
		company.printLinkedHashMap(company.getSecondYearCurrentAsset(), " Second Year Current Assets ");
		System.out.println();
		
		company.printTotalAssets();
		System.out.println();
		
		company.printLinkedHashMap(company.getFirstYearCurrentLiabilities(), "First year current Liabilites");
		System.out.println();
		company.printLinkedHashMap(company.getSecondYearCurrentLiabilities(), "Second year current Liabilites");
		System.out.println();
		
		company.printLinkedHashMap(company.getFirstYearNonCurrentLiabilities(), "First year non-current Liabilites");
		System.out.println();
		company.printLinkedHashMap(company.getSecondYearNonCurrentLiabilities(), "Second year non-current Liabilites");
		System.out.println();
		
		company.printTotalLiabilities();
		System.out.println();
		
		company.printNetAssets();
		System.out.println();
		
		company.printLinkedHashMap(company.getFirstYearEquity(), "First year equity");
		System.out.println();
		company.printLinkedHashMap(company.getSecondYearEquity(), "Second year equity");
		System.out.println();
		
		company.printTotalEquity();
	}
	
	private boolean areEqualsWords(String word1, String word2){
		return DistanceCalculator.getSimilarity(word1, word2)>=0.85;
	}
	
	public void mutchAreIquals(){
		System.out.println("It must be "+mustMutch.size());
		System.out.println("but it has "+justMutch.size());
	}
	
	public boolean fileSucceed(){
		return justMutch.size() >= mustMutch.size();
	}
	
	private int howManyNumbers(int posX, int posY){
		int count = 0;
		Pattern p = Pattern.compile("[+-]?\\d*(,|.)*\\d+");
		for(int j = posY; j < financialPosition[0].length; j++){
		    Matcher m = p.matcher(financialPosition[posX][posY]);
			if(m.matches()){
				count++;
			}
		}
		return count;
	}
	private boolean isNumber(String num){
		Pattern p = Pattern.compile("[+-]?\\d*(,|.)*\\d+");
        Matcher m = p.matcher(num);
        return m.matches();
	}
}
