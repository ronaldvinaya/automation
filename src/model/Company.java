package model;

import java.util.Iterator;
import java.util.Set;
import java.util.LinkedHashMap;

public class Company {
	
	private String nameCompany;
	private int gestions;
	
	private int firstYear;
	private int secondYear;
	
	private int firtsYearPosition;
	private int secondYearPosition;
	
	private NonCurrentAssets nonCurrentAssets;
	private CurrentAssets currentAssets;
	private TotalAssets totalAssets;
	
	private CurrentLiabilities currentLiabilities;
	private NonCurrentLiabilities nonCurrentLiabilities;
	private TotalLiabilities totalLiabilities;
	
	private NetAssets netAssets;
	
	private Equity equity;
	
	private String [][] financeTable;
	
	public Company(String nameCompany, int gestions){
		this.nameCompany = nameCompany;
		this.gestions = gestions;
		nonCurrentAssets = new NonCurrentAssets();
		currentAssets = new CurrentAssets();
		currentLiabilities = new CurrentLiabilities();
		nonCurrentLiabilities = new NonCurrentLiabilities();
		totalLiabilities = new TotalLiabilities();
		equity = new Equity();
	}
	
	public int numGestions(){
		return gestions;
	}
	
	public void setFirstYear(int year){
		firstYear = year;
	}
	
	public void setSecondYear(int year){
		secondYear = year;
	}
	
	public int getFisrtYear(){
		return firstYear;
	}
	
	public int getSecondYear(){
		return secondYear;
	}
	
	public void setTwogestionsPostions(int firtsYearPosition, int secondYearPosition){
		this.firtsYearPosition = firtsYearPosition;
		this.secondYearPosition = secondYearPosition;
	}
	
	public int getFirstManagementPosition(){
		return firtsYearPosition;
	}
	public int getSecondManagementPosition(){
		return secondYearPosition;
	}
	
	public void setFinanceTable(String [][]financeTable){
		this.financeTable = financeTable;
	}
	
	public void addFirstYearNonCurrentAsset(LinkedHashMap<String,String> firstYear){
		nonCurrentAssets.setFisrtYaer(firstYear);
	}
	
	public void addSecondYearNonCurrentAsset(LinkedHashMap<String,String> secondYear){
		nonCurrentAssets.setSecondYear(secondYear);
	}
	
	public void addFirstYearCurrentAsset(LinkedHashMap<String,String> firstYearCurrent){
		currentAssets.setFirstYaer(firstYearCurrent);
	}
	public LinkedHashMap<String,String> getFirstYearCurrentAsset(){
		return currentAssets.getFirstYear();
	}
	
	public void addSecondYearCurrentAsset(LinkedHashMap<String,String> secondYearCurrent){
		currentAssets.setSecondYear(secondYearCurrent);
	}
	public LinkedHashMap<String,String> getSecondYearCurrentAsset(){
		return currentAssets.getSecondYear();
	}
	
	public void addTotalAssets(TotalAssets totalAssets){
		this.totalAssets = totalAssets;
	}
	
	public String getTotalAssestsFirstManagement(){
		return totalAssets.getTotalAssetsFirstYear();
	}
	
	public String getTotalAssetsSecondManagement(){
		return totalAssets.getTotalAssetsSecondYear();
	}
	
	public String getTotalAssetsName(){
		return totalAssets.getName();
	}
	
	public void addFirstYearCurrentLiabilities(LinkedHashMap<String,String> firstYearCurrentLiabilities){
		currentLiabilities.setFisrtYaer(firstYearCurrentLiabilities);
	}
	
	public void addSecondYearCurrentLiabilities(LinkedHashMap<String,String> secondYearCurrentLiabilities){
		currentLiabilities.setSecondYear(secondYearCurrentLiabilities);
	}
	
	public LinkedHashMap<String,String> getSecondYearCurrentLiabilities(){
		return currentLiabilities.getSecondYear();
	}
	
	public LinkedHashMap<String,String> getFirstYearCurrentLiabilities(){
		return currentLiabilities.getFirstYear();
	}
	
	public void addFirstYearNonCurrentLiabilities(LinkedHashMap<String,String> firstYearNonCurrentLiabilities){
		nonCurrentLiabilities.setFisrtYaer(firstYearNonCurrentLiabilities);
	}
	
	public void addSecondYearNonCurrentLiabilities(LinkedHashMap<String,String> secondYearNonCurrentLiabilities){
		nonCurrentLiabilities.setSecondYear(secondYearNonCurrentLiabilities);
	}
	
	public LinkedHashMap<String,String> getSecondYearNonCurrentLiabilities(){
		return nonCurrentLiabilities.getSecondYear();
	}
	
	public LinkedHashMap<String,String> getFirstYearNonCurrentLiabilities(){
		return nonCurrentLiabilities.getFirstYear();
	}

	
	public void addFirstYearEquity(LinkedHashMap<String,String> firstYearEquity){
		equity.setFirstYaer(firstYearEquity);
	}
	public LinkedHashMap<String,String> getFirstYearEquity(){
		return equity.getFirstYear();
	}
	
	public void addSecondYearEquity(LinkedHashMap<String,String> secondYearEquity){
		equity.setSecondYear(secondYearEquity);
	}
	public LinkedHashMap<String,String> getSecondYearEquity(){
		return equity.getSecondYear();
	}
	
	public void addNetAssets(NetAssets netAssets){
		this.netAssets = netAssets;
	}
	
	public String getNetAssetsName(){
		return netAssets.getName();
	}
	
	public String getNetAssetsFirstManagement(){
		return netAssets.getNetAssetsFirstYear();
	}
	
	public String getNetAssetsSecondManagement(){
		return netAssets.getNetAssetsSecondYear();
	}
	
	public void addTotalLiabilities(TotalLiabilities totalLiabilities){
		this.totalLiabilities = totalLiabilities;
	}
	
	public String getTotalLiabilitiesName(){
		return totalLiabilities.getName();
	}
	
	public String getTotalLiabilitiesFirstManagement(){
		return totalLiabilities.getTotalLiabilitiesFirstYear();
	}
	
	public String getTotalLiabilitiesSecondManagement(){
		return totalLiabilities.getTotalLiabilitiesSecondYear();
	}
	

	
	public void setNameTotalEquity(String nameTotalEquity){
		equity.setNameTotalEquity(nameTotalEquity);
	}
	
	public void setTotalEquityFirstYear(String totalEquityFirstYear){
		equity.setTotalEquityFirstYear(totalEquityFirstYear);
	}
	public void setTotalEquitySecondYear(String totalEquitySecondYear){
		equity.setTotalEquitySecondYear(totalEquitySecondYear);
	}
	
	public String getTotalEquityName(){
		return equity.getNameTotalEquity();
	}
	
	public String getTotalEquityFirstYear(){
		return equity.getTotalEquityFirstYear();
	}
	
	public String getTotalEquitySecondYear(){
		return equity.getTotalEquitySecondYear();
	}
	
	public void printTotalEquity(){
		System.out.println("Total Equity "+firstYear);
		System.out.println(equity.getNameTotalEquity()+" "+equity.getTotalEquityFirstYear());
		System.out.println("Total Equity "+secondYear);
		System.out.println(equity.getNameTotalEquity()+" "+equity.getTotalEquitySecondYear());
	}
	
	public void printNetAssets(){
		if(netAssets != null){
			System.out.println("Net Assets "+firstYear);
			System.out.println(netAssets.getName()+ " " +netAssets.getNetAssetsFirstYear());
			System.out.println("Net Assets "+secondYear);
			System.out.println(netAssets.getName()+ " " +netAssets.getNetAssetsSecondYear());
		}else{
			System.out.println("There is not Net Assets");
		}
	}
	
	public void printTotalLiabilities(){
		if(totalLiabilities != null){
			System.out.println("Total Liabilities "+firstYear);
			System.out.println(totalLiabilities.getName()+ " " +totalLiabilities.getTotalLiabilitiesFirstYear());
			System.out.println("Total Liabilities "+secondYear);
			System.out.println(totalLiabilities.getName()+ " " +totalLiabilities.getTotalLiabilitiesSecondYear());
		}else{
			System.out.println("There is not Liabilities");
		}
	}
	
	public void printTotalAssets(){
		System.out.println("Total Assets "+firstYear);
		if(totalAssets.getTotalAssetsFirstYear() != null && totalAssets.getTotalAssetsSecondYear() != null){
			System.out.println(totalAssets.getName()+ " " +totalAssets.getTotalAssetsFirstYear());
			System.out.println("Total Assets "+secondYear);
			System.out.println(totalAssets.getName()+ " " +totalAssets.getTotalAssetsSecondYear());
		}else{
			System.out.println("There is not Total Assets");
		}
	}
	
	public void printFirstYearNonCurrentAsset(){
		System.out.println("Managmnt year "+firstYear+" Non Current Assets");
		LinkedHashMap<String,String> noCurrent = nonCurrentAssets.getFirstYear();
		if(noCurrent != null ){
			Set keys = noCurrent.keySet();
			Iterator ite = keys.iterator(); 
			while(ite.hasNext()){
				String name = ite.next().toString();
		        System.out.print(name);
		        System.out.println(" " + nonCurrentAssets.getFirstYear().get(name));
			}
		}else{
			System.out.println("First Year Non Current Assets is empty!!");
		}
	}
	public void printSecondYearNonCurrentAsset(){
		System.out.println("Managmnt year "+secondYear+" Non Current Assets");
		LinkedHashMap<String,String> noCurrent = nonCurrentAssets.getSecondYear();
		if(noCurrent != null ){
			Set keys = noCurrent.keySet();
			Iterator ite = keys.iterator(); 
			while(ite.hasNext()){
				String name = ite.next().toString();
		        System.out.print(name);
		        System.out.println(" " + nonCurrentAssets.getSecondYear().get(name));
			}
		}else{
			System.out.println("Second Year Non Current Assets is empty!!");
		}
	}
	
	public void printLinkedHashMap(LinkedHashMap<String,String> linkedHashMapData, String message){
		System.out.println("Managmnt for "+message);
		if(linkedHashMapData != null ){
			if(!linkedHashMapData.isEmpty()){
				Set keys = linkedHashMapData.keySet();
				Iterator ite = keys.iterator(); 
				while(ite.hasNext()){
					String name = ite.next().toString();
			        System.out.print(name);
			        System.out.println(" " + linkedHashMapData.get(name));
				}
			}else{
				System.out.println("Your " + message + "is empty!!");
			}
		}else{
			System.out.println("Your " + message + "is empty!!");
		}
	}
}
