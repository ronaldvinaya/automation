package model;

import java.util.LinkedHashMap;

public class CurrentAssets {
	private LinkedHashMap<String,String> firstYear;
	private LinkedHashMap<String,String> secondYear;
	
	public String getTotalCurrentAssetFirsYear(){
		return firstYear.get("total");
	}
	
	public String getTotalCurrentAssetSecondYear(){
		return secondYear.get("total");
	}
	
	public void setFirstYaer(LinkedHashMap<String,String> firstYear){
		this.firstYear = firstYear;
	} 
	
	public void setSecondYear(LinkedHashMap<String,String> secondYear){
		this.secondYear = secondYear;
	}
	
	public LinkedHashMap<String,String> getFirstYear(){
		return firstYear;
	}
	
	public LinkedHashMap<String,String> getSecondYear(){
		return secondYear;
	}
}
