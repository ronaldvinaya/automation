package model;

import java.util.LinkedHashMap;

public class CurrentLiabilities {
	
	private LinkedHashMap<String,String> firstYear;
	private LinkedHashMap<String,String> secondYear;
		
	public void setFisrtYaer(LinkedHashMap<String,String> firstYear){
		this.firstYear = firstYear;
	} 
	
	public void setSecondYear(LinkedHashMap<String,String> secondYear){
		this.secondYear = secondYear;
	}
	
	public LinkedHashMap<String,String> getFirstYear(){
		return firstYear;
	}
	
	public LinkedHashMap<String,String> getSecondYear(){
		return secondYear;
	}
}