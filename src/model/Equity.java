package model;

import java.util.LinkedHashMap;

public class Equity {
	private LinkedHashMap<String,String> firstYear;
	private LinkedHashMap<String,String> secondYear;
	
	private String totalEquityFirstYear;
	private String totalEquitySecondYear;
	
	private String nameTotalEquity;
	
	public void setNameTotalEquity(String nameTotalEquity){
		this.nameTotalEquity = nameTotalEquity;
	}
	
	public String getNameTotalEquity(){
		return nameTotalEquity;
	}
	
	public void setTotalEquityFirstYear(String totalEquityFirstyear){
		this.totalEquityFirstYear = totalEquityFirstyear;
	}
	
	public void setTotalEquitySecondYear(String totalEquitySecondYear){
		this.totalEquitySecondYear = totalEquitySecondYear;
	}
	
	public String getTotalEquitySecondYear(){
		return totalEquitySecondYear;
	}
	
	public String getTotalEquityFirstYear(){
		return totalEquityFirstYear;
	}
	
	public void setFirstYaer(LinkedHashMap<String,String> firstYear){
		this.firstYear = firstYear;
	} 
	
	public void setSecondYear(LinkedHashMap<String,String> secondYear){
		this.secondYear = secondYear;
	}
	
	public LinkedHashMap<String,String> getFirstYear(){
		return firstYear;
	}
	
	public LinkedHashMap<String,String> getSecondYear(){
		return secondYear;
	}
}
