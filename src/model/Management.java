package model;

public class Management {
	private String firstGestion;
	private String secondGestion;
	
	public void setFirstgestion(String firstGestion){
		this.firstGestion = firstGestion;
	}
	public void setSecondGestion(String secondGestion){
		this.secondGestion = secondGestion;
	}
	
	public String getFisrtManagement(){
		return firstGestion;
	}
	public String getSecondManagement(){
		return secondGestion;
	}
	public boolean hasTwoManagements(){
		return firstGestion != null && secondGestion != null;
	}
	
	public boolean hasOneManagement(){
		return firstGestion != null;
	}
	
}
