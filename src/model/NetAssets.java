package model;

public class NetAssets {
	private String netAssetsFirstYear;
	private String netAssetsSecondYear;
	
	private String nameNetAssets;
	
	public void setName(String nameNetAssets){
		this.nameNetAssets = nameNetAssets;
	}
	
	public String getName(){
		return nameNetAssets;
	}
	
	public void setNetAssetsFirstYear(String netAssetsFirstYear){
		this.netAssetsFirstYear = netAssetsFirstYear;
	}
	
	public void setNetAssetsSecondYear(String netAssetsSecondYear){
		this.netAssetsSecondYear = netAssetsSecondYear;
	}
	
	public String getNetAssetsFirstYear(){
		return netAssetsFirstYear;
	}
	
	public String getNetAssetsSecondYear(){
		return netAssetsSecondYear;
	}
}
