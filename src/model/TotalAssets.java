package model;

public class TotalAssets {
	
	private String totalAssetFirstYear;
	private String totalAssetSecondYear;
	
	private String nameTotalAsset;
	
	public void setName(String nameTotalAssets){
		this.nameTotalAsset = nameTotalAssets;
	}
	
	public String getName(){
		return nameTotalAsset;
	}
	
	public void setTotalAssetsFirstYear(String totalAssetFirstYear){
		this.totalAssetFirstYear = totalAssetFirstYear;
	}
	
	public void setTotalAssetsSecondYear(String totalAssetSecondYear){
		this.totalAssetSecondYear = totalAssetSecondYear;
	}
	
	public String getTotalAssetsFirstYear(){
		return totalAssetFirstYear;
	}
	
	public String getTotalAssetsSecondYear(){
		return totalAssetSecondYear;
	}
}
