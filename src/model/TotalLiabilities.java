package model;

public class TotalLiabilities {
	private String totalLiabilitiesFirstYear;
	private String totalLiabilitiesSecondYear;
	
	private String nameTotalAsset;
	
	public void setName(String nameTotalLibilities){
		this.nameTotalAsset = nameTotalLibilities;
	}
	
	public String getName(){
		return nameTotalAsset;
	}
	
	public void setTotalLiabilitiesFirstYear(String totalLiabilitiesFirstYear){
		this.totalLiabilitiesFirstYear = totalLiabilitiesFirstYear;
	}
	
	public void setTotalLiabilitiesSecondYear(String totalLibilitiesSecondYear){
		this.totalLiabilitiesSecondYear = totalLibilitiesSecondYear;
	}
	
	public String getTotalLiabilitiesFirstYear(){
		return totalLiabilitiesFirstYear;
	}
	
	public String getTotalLiabilitiesSecondYear(){
		return totalLiabilitiesSecondYear;
	}
}
