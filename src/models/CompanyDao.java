package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CompanyDao {
	private Session session;
    private Transaction tx;
    
    public int saveCompany(CompanyModel company) throws HibernateException {
        int id = 0;
        
        try {
            startOperation();
            id = (Integer) session.save(company);
            tx.commit();
        } catch (HibernateException he) {
            companyExcepcion(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void companyExcepcion(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Company data " + he);
    }
}
