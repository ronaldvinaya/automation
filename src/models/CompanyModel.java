package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name="company")
public class CompanyModel {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
    @Column(name="name_company")
    private String nameCompany;
    
    @Column(name="file_name")
    private String fileName;
    
    @Column(name="first_management_year")
    private int firstManagementYear;
    
    @Column(name="second_management_year")
    private int secondManagementYear;
    
    
    
    @Column(name="total_assets_name")
    private String totalAssetsName;
    
    @Column(name="total_assets_first_mamagement")
    private String totalAssetsFirstManagement;
    
    @Column(name="total_assets_second_management")
    private String totalAssetsSecondManagement;
    
    
    
    @Column(name="total_liabilities_name")
    private String totalLiabilitiesName;
    
    @Column(name="total_liabilities_first_management")
    private String totalLiabilitiesFirstManagement;
    
    @Column(name="total_liabilities_second_management")
    private String totalLiabilitiesSecondManagement;
    
    
    
    @Column(name="net_assets_name")
    private String netAssetsName;
    
    @Column(name="net_assets_first_management")
    private String netAssetsFirstManagement;
    
    @Column(name="net_assets_second_management")
    private String netAssetsSecondManagement;
    
    

    @Column(name="total_equity_name")
    private String totalEquityName;
    
    @Column(name="total_equity_first_management")
    private String totalEquityFirstManagement;
    
    @Column(name="total_equity_second_management")
    private String totalEquitySecondManagement;
    
    
    
    @ManyToMany
    private List<CurrentAssetsModel> currentAssets = new ArrayList<CurrentAssetsModel>();
    
    @ManyToMany
    private List<NonCurrentAssetsModel> nonCurrentAssets = new ArrayList<NonCurrentAssetsModel>();
    
    @ManyToMany
    private List<CurrentLiabilitiesModel> currentLiabilities = new ArrayList<CurrentLiabilitiesModel>();
    
    @ManyToMany
    private List<NonCurrentLiabilitiesModel> nonCurrentLiabilities = new ArrayList<NonCurrentLiabilitiesModel>();
    
    @ManyToMany
    private List<EquityModel> equity = new ArrayList<EquityModel>();
    
    public CompanyModel(){
    	
    }
    
	public CompanyModel(String nameCompany){
		this.nameCompany = nameCompany;
	}
	
	public void setFirstManagementYear(int year){
		firstManagementYear = year;
	}
	
	public void setSecondManagementYear(int year){
		secondManagementYear = year;
	}
	
	public int getFirstManagementYear(){
		return firstManagementYear;
	}
	
	public int getSecondManagementYear(){
		return secondManagementYear;
	}
	
	public void setNameFile(String fileName){
		this.fileName = fileName;
	}
	
	public String getNameFile(){
		return fileName;
	}
	
	public void addTotalAssetsName(String totalAssetsName){
		this.totalAssetsName = totalAssetsName;
	}
	
	public void addTotalAssetsFirstManagement(String totalAssetsFirstManagement){
		this.totalAssetsFirstManagement = totalAssetsFirstManagement;
	}
	
	public void addTotalAssetsSecondManagement(String totalAssetsSecondManagement){
		this.totalAssetsSecondManagement = totalAssetsSecondManagement;
	}
	
	public String getTotalAssetsSecondManagement(){
		return totalAssetsSecondManagement;
	}
	
	public String getTotalAssetsFirstManagement(){
		return totalAssetsFirstManagement;
	}
	
	public void addFirstYearNonCurrentAsset(NonCurrentAssetsModel firstYear){
		nonCurrentAssets.add(firstYear);
	}
	
	public void addSecondYearNonCurrentAsset(NonCurrentAssetsModel secondYear){
		nonCurrentAssets.add(secondYear);
	}
	
	public List<NonCurrentAssetsModel> getNonCurrentAssets(){
		return nonCurrentAssets;
	}
	
	public void addFirstYearCurrentAsset(CurrentAssetsModel firstYearCurrent){
		currentAssets.add(firstYearCurrent);
	}
	
	public void addSecondYearCurrentAsset(CurrentAssetsModel secondYearCurrent){
		currentAssets.add(secondYearCurrent);
	}
	
	public List<CurrentAssetsModel> getCurrentAssets(){
		return currentAssets;
	}
	
	public void addFirstYearCurrentLiabilities(CurrentLiabilitiesModel firstYearCurrentLiabilities){
		currentLiabilities.add(firstYearCurrentLiabilities);
	}
	
	public void addSecondYearCurrentLiabilities(CurrentLiabilitiesModel secondYearCurrentLiabilities){
		currentLiabilities.add(secondYearCurrentLiabilities);
	}
	
	public List<CurrentLiabilitiesModel> getCurrentLiabilities(){
		return currentLiabilities;
	}
	
	public void addFirstYearNonCurrentLiabilities(NonCurrentLiabilitiesModel firstYearNonCurrentLiabilities){
		nonCurrentLiabilities.add(firstYearNonCurrentLiabilities);
	}
	
	public void addSecondYearNonCurrentLiabilities(NonCurrentLiabilitiesModel secondYearNonCurrentLiabilities){
		nonCurrentLiabilities.add(secondYearNonCurrentLiabilities);
	}
	
	public List<NonCurrentLiabilitiesModel> getNonCurrentLiabilities(){
		return nonCurrentLiabilities;
	}
	
	public void addFirstYearEquity(EquityModel firstYearEquity){
		equity.add(firstYearEquity);
	}
	
	public void addSecondYearEquity(EquityModel secondYearEquity){
		equity.add(secondYearEquity);
	}
	
	public List<EquityModel> getEquity(){
		return equity;
	}
	
	public void addNetAssetName(String netAssetsName){
		this.netAssetsName = netAssetsName;
	}
	
	public void addNetAssetsFirstManagement(String netAssets){
		this.netAssetsFirstManagement = netAssets;
	}
	
	public void addNetAssetsSecondManagement(String netAssets){
		this.netAssetsSecondManagement = netAssets;
	}
	
	public void addTotalLiabilitiesName(String totalLiabilitiesName){
		this.totalLiabilitiesName = totalLiabilitiesName;
	}
	
	public void addTotalLiabilitiesFirstManagement(String totalLiabilities){
		this.totalLiabilitiesFirstManagement = totalLiabilities;
	}
	
	public void addTotalLiabilitiesSecondManagement(String totalLiabilities){
		this.totalLiabilitiesSecondManagement = totalLiabilities;
	}
	
	public void addTotalEquityName(String totalEquityName){
		this.totalEquityName = totalEquityName ;
	}
	
	public void addTotalEquityFirstManagement(String totalEquityFirstYear){
		totalEquityFirstManagement = totalEquityFirstYear;
	}
	
	public void addTotalEquitySecondYear(String totalEquitySecondYear){
		totalEquitySecondManagement = totalEquitySecondYear;
	}
}
