package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CurrentAssetsDao {
	private Session session;
    private Transaction tx;
    
    public long saveCurrentAssets(CurrentAssetsModel currentAssets) throws HibernateException {
        long id = 0;
        
        try {
            startOperation();
            id = (Long) session.save(currentAssets);
            tx.commit();
        } catch (HibernateException he) {
            currentAssetsException(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void currentAssetsException(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Current Assets data " + he);
    }
}
