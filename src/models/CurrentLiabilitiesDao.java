package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class CurrentLiabilitiesDao {
	private Session session;
    private Transaction tx;
    
    public int saveCurrentLiabilities(CurrentLiabilitiesModel currentAssets) throws HibernateException {
        int id = 0;
        
        try {
            startOperation();
            id = (Integer) session.save(currentAssets);
            tx.commit();
        } catch (HibernateException he) {
            currentLiabilitiesException(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void currentLiabilitiesException(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Current Liabilities data " + he);
    }
}
