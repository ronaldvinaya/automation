package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class EquityDao {
	private Session session;
    private Transaction tx;
    
    public int saveEquity(EquityModel equity) throws HibernateException {
        int id = 0;
        
        try {
            startOperation();
            id = (Integer) session.save(equity);
            tx.commit();
        } catch (HibernateException he) {
            equityException(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void equityException(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Equity data " + he);
    }
}
