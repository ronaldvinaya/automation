package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="equity")
public class EquityModel {
	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;
	
    @Column(name="name")
    private String name;
    
    @Column(name="value")
    private String value;
    
    @Column(name="year")
    private int year;
    
    public EquityModel(String name, String value, int year){
    	this.name = name;
    	this.value = value;
    	this.year = year;
    }
}
