package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class NonCurrentAssetsDao {
	private Session session;
    private Transaction tx;
    
    public int saveCurrentAssets(NonCurrentAssetsModel noncurrentAssets) throws HibernateException {
        int id = 0;
        
        try {
            startOperation();
            id = (Integer) session.save(noncurrentAssets);
            tx.commit();
        } catch (HibernateException he) {
            nonCurrentAssetsException(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void nonCurrentAssetsException(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Non Current Assets data " + he);
    }
}
