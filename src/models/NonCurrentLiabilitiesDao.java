package models;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class NonCurrentLiabilitiesDao {
	private Session session;
    private Transaction tx;
    
    public int saveNonCurrentLibilities(NonCurrentLiabilitiesModel nonCurrentLiabilities) throws HibernateException {
        int id = 0;
        
        try {
            startOperation();
            id = (Integer) session.save(nonCurrentLiabilities);
            tx.commit();
        } catch (HibernateException he) {
            nonCurrentLiabilitiesException(he);
            throw he;
        } finally {
            session.close();
        }
        return id;
    }

    private void startOperation() throws HibernateException {
        session = HibernateUtil.getSessionFactory().openSession();
        tx = session.beginTransaction();
    }

    private void nonCurrentLiabilitiesException(HibernateException he) throws HibernateException {
        tx.rollback();
        throw new HibernateException("Occurred an error to access Non Current Liabilities data " + he);
    }
}
